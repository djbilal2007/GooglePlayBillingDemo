package com.googleplay.billingdemo.googleplaybillingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.googleplay.billingdemo.googleplaybillingdemo.util.IabHelper;
import com.googleplay.billingdemo.googleplaybillingdemo.util.IabResult;
import com.googleplay.billingdemo.googleplaybillingdemo.util.Inventory;
import com.googleplay.billingdemo.googleplaybillingdemo.util.Purchase;

public class MainActivity extends AppCompatActivity {

    IabHelper mHelper;
    private Button clickButton, buyButton;
    private static final String ITEM_SKU = "android.test.purchased";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buyButton= (Button)findViewById(R.id.buy_click);
        clickButton = (Button)findViewById(R.id.click_me);
        clickButton.setEnabled(false);

        //Google Public Release HashKey
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkFsGQ/9GHLCoDa0qknYMQFKDjRgqx+a0r+oH9MbmoNVmqr872hIMQgXZdAIm6EKp857CiqNuW2yj6JxbeIhtlngTuvv0hKpS5pDLOd39T/RaWBo2KVEfL1C3umWWGwo/21F/yMCksO8jqf0a8dAUljKgMFaGfc9GzpUY3hyLkP9l5a+StBST68aJJR/VvdgSjGktNzzHqnG9PMX2T1VLZ2yw4XqPKLiO5EWEsZTHfIPFjLHcz8ciWmvaZNQjYkeYD2sa5PPGngmapHH+JLtwD5Sw9OxjPB1QicnXyMXHz6Sfr6nb74JTp6earzOFMYC3Y0ee3KGcw7qUqBaOE8v1LQIDAQAB";

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if(!result.isSuccess()){
                    Log.d("MainActivity", "In-app Billing setup failed. " + result);
                }else {
                    Log.d("MainActivity", "In-app Billing setup is OK.");
                }
            }
        });
    }

    public void buttonClicked(View view){
        clickButton.setEnabled(false);
        buyButton.setEnabled(true);
    }

    public void buyClick(View view) {
        mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001, mPurchaseFinishedListener, "mypurchasetoken");
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if(result.isFailure()){
                return;
            }
            else if(purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                buyButton.setEnabled(false);
            }
        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            if(result.isFailure()){

            }else {
                mHelper.consumeAsync(inv.getPurchase(ITEM_SKU), mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if(result.isSuccess()){
                clickButton.setEnabled(true);
            }else {}
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(mHelper.handleActivityResult(requestCode,resultCode,data)){
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mHelper != null){
            mHelper.dispose();
            mHelper = null;
        }
    }
}

//Constants

/*
    IInAppBillingService mService;
    ServiceConnection mServiceConn;
*/

//Oncreate
/* mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = IInAppBillingService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }
        };

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);*/


//OnDestroy
        /*if(mService != null){
            unbindService(mServiceConn);
        }*/